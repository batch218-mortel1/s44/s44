const mongoose = require("mongoose");
const Product = require("../models/products.js");

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		createdOn: reqBody.createdOn
	})
	
	return newProduct.save().then((newProduct, error) => {
		if(error){
			return error;
		}
		else{
			return newProduct;
		}
	})
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
			return result;
	})
}

module.exports.getProduct = (productId) => {

						//inside the parenthesis should be the id
	return Product.findById(productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
	
		return Product.findByIdAndUpdate(productId, 
			{			 
				productName: newData.product.productName,
				description: newData.product.description,
				price: newData.product.price,
				createdOn: newData.product.createdOn
			}
		).then((updatedProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error){
			return false
		} 
		return {
			message: "Product archived successfully!"
		}
	})
};