const User = require("../models/users.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// module.exports.checkEmailExist = (reqBody) => {

// return User.find({email: reqBody.email }).then(result => {

// 		if(result.length > 0){
// 			return true;
// 		}
	
// 		else
// 		{
// 			return false;
// 		}
// 	})
// }

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
};

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{

				return false;

			}
		}
	})
};

module.exports.getProfileA = (reqBody) => {
	return User.findById(reqBody._id).then((details, err) => {
		if(err){
			return false;
		}
		else{
			details.password = "*****";
			return details;
		}
	})
}

module.exports.getProfileB = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
}


